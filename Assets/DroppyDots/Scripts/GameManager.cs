﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#if AADOTWEEN
using DG.Tweening;
#endif

#if APPADVISORY_LEADERBOARD
using AppAdvisory.social;
#endif

#if APPADVISORY_ADS
using AppAdvisory.Ads;
#endif

namespace AppAdvisory.DroppyDots
{
	/// <summary>
	/// TODO
	/// </summary>
	public class GameManager : MonoBehaviour 
	{
		System.Random rand = new System.Random();

		public int numberOfPlayToShowInterstitial = 5;

		public string VerySimpleAdsURL = "http://u3d.as/oWD";

		public Color red = Color.red;
		public Color blue = Color.blue;
		public Color green = Color.green;
		public Color yellow = Color.yellow;

		public float speedMinInSeconds = 0.3f;
		public float speedMaxInSeconds = 1.5f;

		public DColor[] colors;

		public Text inGameScore;
		public Text lastScore;
		public Text bestScore;

		public Image title;
		public Image instruction;

		public Transform dotPrefab;

		int _point = 0;
		int point
		{
			get
			{
				return _point;
			}

			set
			{
				_point = value;

				inGameScore.text = _point.ToString();
			}
		}

		public AudioClip[] pocs;
		public AudioClip lose;

		public CanvasGroup[] canvasGroups;

		[System.NonSerialized] public bool isGameOver = false;

		void Awake()
		{
			print("AWAKE");
			isGameOver = true;

			Application.targetFrameRate = 60;

			ResetUIElement();

			transform.position = Vector3.zero;

			lastScore.text = "Last\n" + Utils.GetLast();
			bestScore.text = "Best\n" + Utils.GetBest();

			if(Time.realtimeSinceStartup < 3)
            {
#if DOTween
                Debug.Log("est");
				DOTween.Init();
#endif
            }

		}

		void Start()
		{
			Color titleColor = title.color;
			titleColor.a = 0;
			title.color = titleColor;

			if(canvasGroups != null)
			{
				foreach(var c in canvasGroups)
				{
					SetCanvasGroupAlpha(c);
				}
			}


			#if AADOTWEEN
			title.DOFade(1,1)
				.SetEase(Ease.Linear)
				.OnUpdate( () => {
					if(canvasGroups != null)
					{
						foreach(var c in canvasGroups)
						{
							SetCanvasGroupAlpha(c);
						}
					}
				})
				.OnComplete(() => {
					titleColor.a = 1;
					title.color = titleColor;
					if(canvasGroups != null)
					{
						foreach(var c in canvasGroups)
						{
							SetCanvasGroupAlpha(c);
						}
					}
				});
			#endif
		}

		void PlaySoundPoc()
		{
			int i = rand.Next(0,pocs.Length);
			GetComponent<AudioSource>().PlayOneShot(pocs[i]);
		}

		void PlaySoundLose()
		{
			GetComponent<AudioSource>().PlayOneShot(lose);
		}

		void ResetUIElement()
		{
			isGameOver = true;

			Color c = title.color;
			c.a = 1;
			title.color = c;

			c = instruction.color;
			c.a = 0;
			instruction.color = c;

			c = inGameScore.color;
			c.a = 0;
			inGameScore.color = c;

			inGameScore.text = "";
			#if AADOTWEEN
			inGameScore.DOFade(0.5f,1);
			#endif
		}

		public void DOStart()
		{
			FindObjectOfType<Floor>().DODisable();

			#if AADOTWEEN
			DOVirtual.DelayedCall(1, () => {
				isGameOver = false;

				FindObjectOfType<Floor>().DODisable();
				FindObjectOfType<Floor>().DOEnable();
			});
			#endif
			DOCreateDot();
			#if AADOTWEEN
			title.DOKill();
			instruction.DOKill();
			inGameScore.DOKill();

			title.DOFade(0,1)
				.OnUpdate( () => {
					if(canvasGroups != null)
					{
						foreach(var c in canvasGroups)
						{
							SetCanvasGroupAlpha(c);
						}
					}
				});
			#endif
			#if AADOTWEEN
			instruction.DOFade(1,1)
				.OnComplete(() => {
					instruction.DOFade(0,1).SetDelay(3);
				});

			inGameScore.DOFade(0.0f,1)
				.OnComplete(()=>{
					point = 0;
					inGameScore.DOFade(0.5f,1);
				});
			#endif
		}

		void SetCanvasGroupAlpha(CanvasGroup c)
		{
			if(c != null)
			{
				c.alpha = title.color.a;
				if(c.alpha < 1f)
				{
					c.blocksRaycasts = false;
					c.interactable = false;
				}
				else
				{
					c.blocksRaycasts = true;
					c.interactable = true;
				}
			}
		}


		void DOCreateDot()
		{
			var inst = Instantiate(dotPrefab) as Transform;

			inst.parent = transform;

			inst.GetComponent<DBase>().SetColor(colors[rand.Next(0,colors.Length)]);

			inst.transform.position = new Vector3(FindObjectOfType<Floor>().GetPositionForDot(), 2f * Camera.main.orthographicSize, 0);

			Invoke("DOCreateDot",UnityEngine.Random.Range(speedMinInSeconds,speedMaxInSeconds));

		}

		public void GameOver()
		{
			isGameOver = true;

			FindObjectOfType<Floor>().DODisable();

			ShowAds();

			PlaySoundLose();

			Utils.SetLast(point);
			Utils.SetBest(point);

			ReportScoreToLeaderboard(point);

			lastScore.text = "Last\n" + Utils.GetLast();
			bestScore.text = "Best\n" + Utils.GetBest();


			var dots = FindObjectsOfType<Dot>();

			foreach(var d in dots)
			{
				if(d != null && d.gameObject != null)
					Destroy(d.gameObject);
			}

			CancelInvoke("DOCreateDot");

			#if AADOTWEEN
			title.DOKill();
			instruction.DOKill();
			inGameScore.DOKill();

			DOVirtual.DelayedCall(1,() => {
				isGameOver = true;

				Utils.ReloadLevel();
			});
			#endif
		}


		public void Add1Point()
		{
			point++;

			PlaySoundPoc();
		}

		public void ShowAds()
		{
			int count = PlayerPrefs.GetInt("GAMEOVER_COUNT",0);
			count++;
			PlayerPrefs.SetInt("GAMEOVER_COUNT",count);
			PlayerPrefs.Save();

			#if APPADVISORY_ADS
			if(count > numberOfPlayToShowInterstitial)
			{
			#if UNITY_EDITOR
			print("count = " + count + " > numberOfPlayToShowINterstitial = " + numberOfPlayToShowInterstitial);
			#endif
			if(AdsManager.instance.IsReadyInterstitial())
			{
			#if UNITY_EDITOR
				print("AdsManager.instance.IsReadyInterstitial() == true ----> SO ====> set count = 0 AND show interstial");
			#endif
				PlayerPrefs.SetInt("GAMEOVER_COUNT",0);
				AdsManager.instance.ShowInterstitial();
			}
			else
			{
			#if UNITY_EDITOR
				print("AdsManager.instance.IsReadyInterstitial() == false");
			#endif
			}

		}
		else
		{
			PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
		}
		PlayerPrefs.Save();
			#else
		if(count >= numberOfPlayToShowInterstitial)
		{
			Debug.LogWarning("To show ads, please have a look to Very Simple Ad on the Asset Store, or go to this link: " + VerySimpleAdsURL);
			Debug.LogWarning("Very Simple Ad is already implemented in this asset");
			Debug.LogWarning("Just import the package and you are ready to use it and monetize your game!");
			Debug.LogWarning("Very Simple Ad : " + VerySimpleAdsURL);
			PlayerPrefs.SetInt("GAMEOVER_COUNT",0);
		}
		else
		{
			PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
		}
		PlayerPrefs.Save();
			#endif
	}

	/// <summary>
	/// If using Very Simple Leaderboard by App Advisory, report the score : http://u3d.as/qxf
	/// </summary>
	void ReportScoreToLeaderboard(int p)
	{
		#if APPADVISORY_LEADERBOARD
		LeaderboardManager.ReportScore(p);
		//		#else
		//		print("Get very simple leaderboard to use it : http://u3d.as/qxf");
		#endif
	}


}
}