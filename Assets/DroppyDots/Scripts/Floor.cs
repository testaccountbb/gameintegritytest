﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;
#if AADOTWEEN
using DG.Tweening;
#endif

namespace AppAdvisory.DroppyDots
{
	public class Floor : MonoBehaviour 
	{
		public Transform floorCenter;

		[NonSerialized] public int number = 10;

		public float size = 1;

		System.Random rand = new System.Random();

		float timeMove = 0.07f;

		GameManager _gm;

		void Awake()
		{
			_gm = FindObjectOfType<GameManager>();

			transform.position = Vector3.zero;

			size = floorCenter.GetComponent<SpriteRenderer>().bounds.size.x;

			for(int i = - number; i <= number; i++)
			{
				Transform t = Instantiate(floorCenter) as Transform;
				t.parent = transform;
				var x = size * i;
				//			t.localPosition = new Vector3(x,0,0);

				GameManager gm = FindObjectOfType<GameManager>();

				t.localPosition = new Vector3(x,0,0);
				t.GetComponent<DBase>().SetColor(gm.colors[(i + number + 1)%4]);

				t.name = i.ToString();
			}

			for(int i = 0; i < transform.childCount; i++)
			{
				transform.SetSiblingIndex(i);
			}


			float height = 2f * Camera.main.orthographicSize;

			transform.position = Vector3.up * (-height / 3f);

			DODisable();
		}

		public float GetPositionForDot()
		{
			int max = 4;

			Camera cam = Camera.main;
			float height = 2f * cam.orthographicSize;
			float width = height * cam.aspect;

			if(4 * size > width / 2f)
			{
				max = 3;
			}
			float r = rand.Next(-max,max + 1);

			return r * size;
		}

		public void DOEnable()
		{
			print("DOENABLE");
			DODisable();
			InputTouch.OnTouched += OnTouched;
		}

		public void DODisable()
		{
			InputTouch.OnTouched -= OnTouched;
		}

		void OnDisable()
		{
			DODisable();
		}

		void OnTouched (TouchDirection td)
		{
			if(_gm.isGameOver)
			{
				InputTouch.OnTouched -= OnTouched;
				return;
			}

			if(td == TouchDirection.none)
				return;

			if(td == TouchDirection.left)
				OnTouchLeft();
			else
				OnTouchRight();
		}

		#if AADOTWEEN
		Tween delayedTweener;
		#endif

		void OnTouchLeft()
		{
			#if AADOTWEEN
			if(delayedTweener != null)
				delayedTweener.Kill();
			#endif

			var t = transform.GetChild(0);

			DColor dc = transform.GetChild(transform.childCount - 1).GetComponent<DBase>().color;

			switch(dc)
			{
			case DColor.BLUE:
				t.GetComponent<DBase>().SetColor(DColor.GREEN);
				break;
			case DColor.GREEN:
				t.GetComponent<DBase>().SetColor(DColor.YELLOW);
				break;
			case DColor.YELLOW:
				t.GetComponent<DBase>().SetColor(DColor.RED);
				break;
			case DColor.RED:
				t.GetComponent<DBase>().SetColor(DColor.BLUE);
				break;
			default:
				Debug.LogError("error");
				break;
			}

			t.localPosition = transform.GetChild(transform.childCount - 1).localPosition;
			t.SetSiblingIndex(transform.childCount - 1);

			#if AADOTWEEN
			for(int i = 0; i < transform.childCount - 1; i++)
			{
				var tt = transform.GetChild(i);
				tt.DOKill(true);
				tt.DOLocalMoveX(tt.localPosition.x - size, timeMove).SetEase(Ease.Linear);
			}
			#endif

			InputTouch.OnTouched -= OnTouched;
			#if AADOTWEEN
			delayedTweener = DOVirtual.DelayedCall(0.11f,() => {
				InputTouch.OnTouched += OnTouched;
			});
			#endif
		}

		void OnTouchRight()
		{
			#if AADOTWEEN
			if(delayedTweener != null)
				delayedTweener.Kill();
			#endif

			var t = transform.GetChild(transform.childCount - 1);

			DColor dc = transform.GetChild(0).GetComponent<DBase>().color;

			switch(dc)
			{
			case DColor.BLUE:
				t.GetComponent<DBase>().SetColor(DColor.RED);
				break;
			case DColor.RED:
				t.GetComponent<DBase>().SetColor(DColor.YELLOW);
				break;
			case DColor.YELLOW:
				t.GetComponent<DBase>().SetColor(DColor.GREEN);
				break;
			case DColor.GREEN:
				t.GetComponent<DBase>().SetColor(DColor.BLUE);
				break;
			default:
				Debug.LogError("error");
				break;
			}

			t.localPosition = transform.GetChild(0).localPosition;
			t.SetSiblingIndex(0);

			#if AADOTWEEN
			for(int i = 1; i < transform.childCount; i++)
			{
				var tt = transform.GetChild(i);
				tt.DOKill(true);
				tt.DOLocalMoveX(tt.localPosition.x + size, timeMove).SetEase(Ease.Linear);
			}
			#endif

			InputTouch.OnTouched -= OnTouched;

			#if AADOTWEEN
			delayedTweener = DOVirtual.DelayedCall(0.11f,() => {
				InputTouch.OnTouched += OnTouched;
			});
			#endif
		}

	}
}