﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;

#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

#if AADOTWEEN
using DG.Tweening;
#endif

namespace AppAdvisory.DroppyDots
{
	public static class Utils
	{
		public static bool HaveSameColor(this DBase dBase, DBase other)
		{
			return dBase.color == other.color;
		}

		public static bool HaveSameColor(this Transform dBase, Transform other)
		{
			return dBase.GetComponent<DBase>().color == other.GetComponent<DBase>().color;
		}

		public static bool HaveSameColor(this Collider2D c, Transform other)
		{
			return c.GetComponent<DBase>().color == other.GetComponent<DBase>().color;
		}

		public static bool HaveSameColor(this Collider2D c, DBase other)
		{
			return c.GetComponent<DBase>().color == other.color;
		}

		public static bool HaveSameColor(this DColor c, Transform other)
		{
			return c == other.GetComponent<DBase>().color;
		}

		public static void SetLast(int lastScore)
		{
			PlayerPrefs.SetInt("LAST_SCORE",lastScore);
			PlayerPrefs.Save();

		}

		public static int GetLast()
		{
			return PlayerPrefs.GetInt("LAST_SCORE", 0);
		}

		public static bool SetBest(int lastScore)
		{
			int best = GetBest();

			if(lastScore > best)
			{
				PlayerPrefs.SetInt("BEST_SCORE",lastScore);
				PlayerPrefs.Save();
				return true;
			}

			return false;
		}

		public static int GetBest()
		{
			return PlayerPrefs.GetInt("BEST_SCORE",0);
		}

		/// <summary>
		/// Clean the memory and reload the scene
		/// </summary>
		public static void ReloadLevel()
		{
			CleanMemory();

			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadSceneAsync(0,LoadSceneMode.Single);
			#else
			Application.LoadLevel(Application.loadedLevel);
			#endif

			CleanMemory();
		}
		/// <summary>
		/// Clean the memory
		/// </summary>
		public static void CleanMemory()
		{
			#if AADOTWEEN
			DOTween.KillAll();
			#endif

			GC.Collect();
			Application.targetFrameRate = 60;
		}
	}
}